(ns double-booked.core-test
  (:require [clojure.test :refer :all]
            [double-booked.core :refer :all]))


(deftest overlaps-test
  (testing "An event overlaps with other event having same start and end"
    (let [event {:start 2 :end 2}]
      (is (overlaps? event event))))

  (testing "e1, e2 overlap if e2 starts before e1 has ended"
    (let [e1 {:start 10 :end 30}
          e2 {:start 20 :end 50}]
      (is (overlaps? e1 e2))))

  (testing "e1, e2 overlap if e2 starts before e1 has ended and ends after e1 ends"
    (let [e1 {:start 10 :end 30}
          e2 {:start 20 :end 50}]
      (is (overlaps? e1 e2))))

  (testing "e1, e2 overlap if e2 starts and ends between start and end of e1"
    (let [e1 {:start 10 :end 100}
          e2 {:start 20 :end 70}]
      (is (overlaps? e1 e2))))


  (testing "e1, e2 does not overlap if e2 starts after e1 has ended"
    (let [e1 {:start 10 :end 30}
          e2 {:start 40 :end 50}]
      (is (not (overlaps? e1 e2)))))

  (testing "e1, e2 does not overlap if e2 has started and ended before e1 starts"
    (let [e1 {:start 50 :end 100}
          e2 {:start 30 :end 40}]
      (is (not (overlaps? e1 e2))))))

(deftest find-overlaps-test

  (testing "Overlapping events are empty if there are no events"
    (is (empty? (find-overlaps []))))

  (testing "Overlapping events are empty if there is only one event"
    (is (empty? (find-overlaps [{:start 1 :end 2}]))))

  (testing "Overlapping events are empty if all events are independent"
    (let [events [{:start 10 :end 50}
                  {:start 51 :end 100}
                  {:start 101 :end 150}
                  {:start 151 :end 200}]]
      (is (empty? (find-overlaps events)))))

  (testing "Overlapping events"
    (let [events [{:start 10 :end 50}
                  {:start 20 :end 40}
                  {:start 30 :end 70}
                  {:start 100 :end 120}]]
      (is (= [[{:start 10 :end 50} {:start 20 :end 40}]
              [{:start 10 :end 50} {:start 30 :end 70}]
              [{:start 20 :end 40} {:start 30 :end 70}]]
             (find-overlaps events))))))
