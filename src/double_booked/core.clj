(ns double-booked.core
  (:gen-class))

(defn overlaps? [event-1 event-2]
  (let [start-1 (:start event-1)
        end-1   (:end event-1)
        start-2 (:start event-2)
        end-2   (:end event-2)]
    (if (> start-1 start-2)
      (overlaps? event-2 event-1)
      (not (and (< start-1 start-2)
                (< end-1 start-2))))))

(defn find-overlaps [events]
  (let [events-idx (zipmap events (range))]
    (->> (for [[event1 idx1] events-idx
               [event2 idx2] events-idx
               :when (< idx1 idx2)]
           (when (overlaps? event1 event2)
             [event1 event2]))
         (remove nil?))))
